var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.GetAll = function(callback){
    connection.query('SELECT * FROM genree;',
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
    //var qry = "SELECT distinct genre from genres;"
    //connection.query(qry, function(err, result){
    //    callback(err, result);
    //});
}

exports.GetMovieAll = function(callback){
    connection.query('SELECT * FROM genres;',
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
}

exports.Insert = function(genre, callback) {
    var qry = "INSERT INTO genree (genre) VALUES (?)";
    connection.query(qry, [genre], function(err, result){
       callback(err, result);
    });
}