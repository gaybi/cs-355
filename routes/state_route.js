var express = require('express');
var router = express.Router();
var stateDal = require('../model/state_dal');

router.get('/all', function(req, res) {
    stateDal.GetAll(function (err, result) {
            if (err) {
                res.send("Error" + err.message);
            }
            else {
                res.render('displayAllStates.ejs', {rs: result});
            }
        }
    );
});

router.get('/new', function(req, res) {
    res.render('states/state_insert_form');
});

router.get('/state_insert', function(req, res){
    stateDal.Insert(req.query.state, function(err, result){
        var response = {};
        if(err) {
            response.message = err.message;
        }
        else {
            response.message = 'Success!';
        }
        res.json(response);
    });
});

router.get('/edit', function(req, res){
    console.log('/edit state_id:' + req.query.state_id);

    stateDal.GetByID(req.query.state_id, function(err, state_result){
        if(err) {
            console.log(err);
            res.send('error: ' + err);
        }
        else {
            console.log(state_result);
            res.render('states/state_edit_form.ejs', {rs: state_result, message: req.query.message});
        }
    });
});

// Added for Lab 10

router.post('/update_state', function(req,res){
    console.log(req.body);
    // first update the movie
    stateDal.UpdateState(req.body.state_id, req.body.state,
        function(err){
            var message;
            if(err) {
                console.log(err);
                message = 'error: ' + err.message;
            }
            else {
                message = 'success';
            }
            // next update the genres
            stateDal.GetByID(req.body.state_id, function(err, state_info){
                    console.log(state_info);
                    res.redirect('/states/edit?state_id=' + req.body.state_id + '&message=' + message);
                    res.render('states/state_edit_form', {rs: state_info});
            });

        });
});

module.exports = router;