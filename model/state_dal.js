var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.GetAll = function(callback){
    connection.query('SELECT * FROM statee;',
    function (err, result) {
        if(err) {
            console.log(err);
            callback(true);
            return;
        }
        console.log(result);
        callback(false, result);
    }
    );
}

exports.GetUserAll = function(callback){
    connection.query('SELECT * FROM states;',
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
}

exports.Insert = function(state, callback) {
    var qry = "INSERT INTO statee (state) VALUES (?)";
    connection.query(qry, [state], function(err, result){
        callback(err, result);
    });
}

exports.Update = function(user_id, state_id, callback){
    var ans = [state_id, user_id];
    var qry = "UPDATE states SET state_id = ? WHERE user_id =?";
    connection.query(qry, ans, function(err, result){
        callback(err, result);
    });
}

exports.GetByID = function(state_id, callback) {
    console.log(state_id);
    var query = 'SELECT * FROM states WHERE state_id=' + state_id;
    console.log(query);
    connection.query(query,
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            callback(false, result);
        }
    );
};

exports.UpdateState = function(state_id, state, callback){
    var ans = [state, state_id];
    var qry = "UPDATE states SET state = ? WHERE state_id =?";
    connection.query(qry, ans, function(err, result){
        callback(err, result);
    });
}