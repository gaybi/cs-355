var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.GetAll = function(callback) {
    connection.query('SELECT * FROM user_info_view;',
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
}


exports.GetByID = function(user_id, callback) {
    console.log(user_id);
    var query = 'SELECT * FROM user_movie_ratings WHERE user_id = ' + user_id + ';';
    console.log(query);
    connection.query(query,
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            callback(false, result);
        }
    );
}

var DeleteUserStates = function(user_id, callback) {
    var state_qry = 'DELETE FROM states WHERE user_id = ?';
    connection.query(state_qry, user_id, function (err, result) {
        callback(err, result);
    });
};

var AddUserStates = function(user_id, state_ids, callback) {
    if (state_ids != null) {
        var state_qry_values = [];

        if (state_ids instanceof Array) {
            for (var i = 0; i < state_ids.length; i++) {
                var theState = 'SELECT state from statee WHERE state_id=' + state_ids[i];
                state_qry_values.push([user_id, state_ids[i], theState]);
            }
        }
        else {
            var theState = 'SELECT state from statee WHERE state_id=' + state_ids;
            state_qry_values.push([user_id, state_ids, theState]);
        }
        var state_qry = 'INSERT INTO states (user_id, stateid) VALUES ?';
        connection.query(state_qry, [state_qry_values], function (err) {
            callback(err);
        });
    }
};

exports.Update = function(user_id, username, first_name, last_name, email, password, state_id, callback) {
    console.log(user_id, username, first_name, last_name, email, password, state_id);
    var values = [username, first_name, last_name, email, password, user_id];

    connection.query('UPDATE users SET username = ?, first_name = ?, last_name = ?, email = ?, password = ? WHERE user_id = ?', values,
        function(err, result){
            if(err) {
                console.log(this.sql);
                callback(err, null);
            }
            else {
                // delete all the existing genres for the movie first
                DeleteUserStates(user_id, function(err, result) {
                    //then add them back in.
                    AddUserStates(user_id, state_id, callback);
                });
            }
        });
}

var Delete = function(user_id, callback) {
//function Delete(movie_id, callback) {
    var qry = 'DELETE FROM users WHERE user_id = ?';
    connection.query(qry, [user_id],
        function (err) {
            callback(err);
        });
}

exports.DeleteById = Delete;