var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.GetAll = function(callback) {
    connection.query('SELECT * FROM users;',
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
}


exports.GetByID = function(user_id, callback) {
    console.log(user_id);
    var query = 'SELECT * FROM users WHERE user_id=' + user_id;
    console.log(query);
    connection.query(query,
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            callback(false, result);
        }
    );
}

/* NOTE: Just like functions in other languages the parameters sent to a function do not need to have the same names
 as the function definition. In this case, I'm sending the "req" parameter from the router to exports.Insert(),
 but exports.Insert() has defined this input parameter as "account_info"

 the callback parameter is an anonymous function passed like a parameter, that will be run later on within
 this function.

exports.Insert = function(users_info, callback) {

    console.log(users_info);

    var dynamic_query = 'INSERT INTO users (first_name, last_name, email, password, state_id) VALUES (' +
        '\'' + users_info.first_name + '\', ' +
        '\'' + users_info.last_name + '\', ' +
        '\'' + users_info.email + '\', ' +
        '\'' + users_info.password + '\', ' +
        '\'' + users_info.state_id + '\'' +
        ');';

    console.log("test");
    console.log(dynamic_query);

    connection.query(dynamic_query,

        function (err, result) {

            if(err) {

                console.log(err);
                callback(true);
                return;
            }
            callback(false, result);
        }
    );
}
*/

exports.Insert = function(username, first_name, last_name, email, password, states, callback) {
    var values = [username, first_name, last_name, email, password];
    connection.query('INSERT INTO users (username, first_name, last_name, email, password) VALUES (?, ?, ?, ?, ?)', values,
        function (err, result) {

            if (err == null && states != null) {
                var state_qry_values = [];

                if(states instanceof Array) {
                    for (var i = 0; i < states.length; i++) {
                        state_qry_values.push([result.insertId, states[i]]);
                    }
                }
                else {
                    state_qry_values.push([result.insertId, Number(states)]);
                }
                console.log(state_qry_values);
                var state_qry = 'INSERT INTO states (user_id, stateid) VALUES ?';
                connection.query(state_qry, [state_qry_values], function(err, state_result){
                    if(err) {
                        Delete(result.insertId, function() {
                            callback(err);
                        });
                    }
                    else {
                        callback(err);
                    }
                });
            }
            else {
                callback(err);
            }
        });
}

var DeleteUserStates = function(user_id, callback) {
    var state_qry = 'DELETE FROM states WHERE user_id = ?';
    connection.query(state_qry, Number(user_id), function (err, result) {
        callback(err, result);
    });
};

var AddUserStates = function(user_id, state_id, callback) {
    if (state_id != null) {
        var state_qry_values = [];

        if (state_id instanceof Array) {
            for (var i = 0; i < state_id.length; i++) {
                var theState = 'SELECT state from statee WHERE state_id=' + state_id[i];
                state_qry_values.push([user_id, state_id[i], theState]);
            }
        }
        else {
            var theState = 'SELECT state from statee WHERE state_id=' + state_id;
            state_qry_values.push([user_id, state_id, theState]);
        }
        var state_qry = 'INSERT INTO states (user_id, stateid) VALUES ?';
        connection.query(state_qry, [state_qry_values], function (err) {
            callback(err);
        });
    }
};

exports.Update = function(user_id, username, first_name, last_name, email, password, state_id, callback) {
    console.log(Number(user_id), username, first_name, last_name, email, password, state_id);
    var values = [username, first_name, last_name, email, password, user_id];
    var theQry = 'UPDATE users SET username = ?, first_name = ?, last_name = ? , email = ?, password = ? WHERE user_id = ?';
    connection.query(theQry, values,
        function(err, result){
            if(err) {
                console.log(this.sql);
                callback(err, null);
            }
            else {
                // delete all the existing states for the user first
                DeleteUserStates(user_id, function(err, result) {
                    //then add them back in.
                    AddUserStates(user_id, state_id, callback);
                });
            }
        });
    
}

var Delete = function(user_id, callback) {
//function Delete(user_id, callback) {
    var qry = 'DELETE FROM users WHERE user_id = ?';
    connection.query(qry, [Number(user_id)],
        function (err) {
            callback(err);
        });
}

exports.DeleteById = Delete;