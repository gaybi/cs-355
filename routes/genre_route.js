var express = require('express');
var router = express.Router();
var genreDal = require('../model/genre_dal');

router.get('/all', function(req, res) {
    genreDal.GetAll(function (err, result) {
            if (err) {
                res.send("Error" + err.message);
            }
            else {
                res.render('displayAllGenres.ejs', {rs: result});
            }
        }
    );
});


router.get('/new', function(req, res) {
    /*genreDal.GetAll( function(err, result){
        if(err) {
            res.send("Error: " + err);
        }
        else {
            res.render('genre/genre_insert_form.ejs', {genre: result});
        }
    });*/
    res.render('genre/genre_insert_form');
});

router.get('/genre_insert', function(req, res){
    //console.log(req.query.genre);
    genreDal.Insert(req.query.genre, function(err, result){
           var response = {};
           if(err) {
               response.message = err.message;
           }
           else {
               response.message = 'Success!';
           }
           res.json(response);
        });
});


module.exports = router;