var express = require('express');
var router = express.Router();
var userDal = require('../model/user_dal');
var usersDal = require('../model/users_dal');
var stateDal = require('../model/state_dal');

/* GET users listing.
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
}); */

router.get('/save', function(req, res, next) {
    console.log("username is: " + req.query.username);
    console.log("firstname equals: " + req.query.first_name);
    console.log("the lastname submitted was: " + req.query.last_name);
    console.log("email is: " + req.query.email);
    console.log("password is: " + req.query.password);
    console.log("state id is: " + req.query.state_id);
    usersDal.Insert(req.query, function(err, result) {
        if (err) {
            res.send(err);
        }
        else {
            res.send("Successfully saved the user.");
        }
    });
});

router.get('/create', function(req, res) {
    usersDal.GetAll( function(err, result){
        if(err) {
            res.send("Error: " + err);
        }
        else {
            res.render('userFormCreate.ejs', { title: 'Create'});
        }
    });
});

router.get('/new', function(req, res) {
    usersDal.GetAll( function(err, result){
        if(err) {
            res.send("Error: " + err);
        }
        else {
            stateDal.GetAll(function(err, state_result) {
                console.log(state_result);
                res.render('userFormCreate.ejs', {states: state_result});
            })
        }
    });

});

router.post('/insert_user', function(req, res) {
    console.log(req.body);
    usersDal.Insert(req.body.username, req.body.first_name, req.body.last_name, req.body.email, req.body.password, req.body.state_id,
        function(err){
            if(err){
                res.send('Fail!<br />' + err);
            } else {
                res.send('Success!')
            }
        });
});


router.get('/all', function(req, res) {
  usersDal.GetAll(function (err, result) {
        if (err) throw err;
        res.render('displayAllUsers.ejs', {rs: result});
      }
  );
});

router.get('/', function (req, res) {
  userDal.GetByID(req.query.user_id, function (err, result) {
        if (err) throw err;

        res.render('displayUserInfo.ejs', {rs: result, user_id: req.query.user_id});
      }
  );
});

router.get('/edit', function(req, res){
    console.log('/edit user_id:' + req.query.user_id);

    usersDal.GetByID(req.query.user_id, function(err, user_result){
        if(err) {
            console.log(err);
            res.send('error: ' + err);
        }
        else {
            console.log(user_result);
            stateDal.GetAll(function(err, state_result){
                console.log(state_result);
                res.render('users/user_edit_form.ejs', {rs: user_result, states: state_result, message: req.query.message});
            });
        }
    });
});

// Added for Lab 10

router.post('/update_user', function(req,res){
    console.log(req.body);
    // first update the user
    usersDal.Update(req.body.user_id, req.body.username, req.body.first_name, req.body.last_name, req.body.email,
        req.body.password, req.body.state_id,
        function(err){
            var message;
            if(err) {
                console.log(err);
                message = 'error: ' + err.message;
            }
            else {
                message = 'success';
            }
            // next update the states
            /*stateDal.Update(req.body.user_id, req.body.state_id,
                function(err) {
                    var message;
                    if (err) {
                        console.log(err);
                        message = 'error: ' + err.message;
                    }
                    else {
                        message = 'success';
                    }
                });*/
            userDal.GetByID(req.body.user_id, function(err, user_info){
                stateDal.GetUserAll(function(err, state_result){
                    //console.log(user_info);
                    console.log(state_result);
                    //res.send('Successfully Updated');
                    res.redirect('/users/edit?user_id=' + req.body.user_id + '&message=' + message);
                    res.render('users/user_edit_form', {rs: user_info, states: state_result, message: message});
                });
            });

        });
});

// Added for Lab 10

router.get('/delete', function(req, res){
    console.log(req.query);
    usersDal.GetByID(req.query.user_id, function(err, result) {
        if(err){
            res.send("Error: " + err);
        }
        else if(result.length != 0) {
            userDal.DeleteById(req.query.user_id, function (err) {
                res.send(result[0].username + ' Successfully Deleted');
            });
        }
        else {
            res.send('User does not exist in the database.');
        }
    });
});

module.exports = router;